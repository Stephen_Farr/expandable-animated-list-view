package com.android.expandableanimatedrecyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.LinkedList;

/**
 * Created by Stephen Farr on 5/5/2015.
 */
public class EARVAdapter extends RecyclerView.Adapter<EARVListItemViewHolder> implements EARVListItemExpansionDelegate{

    private final Context context;
    private final LinkedList<Boolean> expanded;

    public EARVAdapter(final Context context) {
        this.context = context;
        setHasStableIds(true);

        this.expanded = new LinkedList<Boolean>();
        for (int i = 0; i < this.getItemCount(); i++) {
            this.expanded.add(false);
        }
    }

    @Override
    public EARVListItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(this.context).inflate(R.layout.view_listitem, viewGroup, false);
        EARVListItemViewHolder viewHolder = new EARVListItemViewHolder(view);
        viewHolder.delegate = this;

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(EARVListItemViewHolder viewHolder, int position) {}

    @Override
    public void onViewAttachedToWindow(EARVListItemViewHolder viewHolder) {
        super.onViewAttachedToWindow(viewHolder);

        viewHolder.setViewExpanded(this.expanded.get(viewHolder.getPosition()));
    }

    @Override
    public int getItemCount() {
        return 20;
    }

    @Override
    public void itemStateChanged(EARVListItemViewHolder viewHolder, boolean expanded) {
        this.expanded.remove(viewHolder.getPosition());
        this.expanded.add(viewHolder.getPosition(), expanded);
    }
}
