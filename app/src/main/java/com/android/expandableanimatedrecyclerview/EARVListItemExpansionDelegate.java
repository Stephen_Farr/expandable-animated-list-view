package com.android.expandableanimatedrecyclerview;

/**
 * Created by Stephen Farr on 5/6/2015.
 */
public interface EARVListItemExpansionDelegate {
    void itemStateChanged(final EARVListItemViewHolder viewHolder, final boolean expanded);
}
