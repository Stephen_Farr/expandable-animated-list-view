package com.android.expandableanimatedrecyclerview;

import android.animation.ValueAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Stephen Farr on 5/5/2015.
 */
public class EARVListItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public View rootView;
    public TextView alwaysShownTextView;
    public TextView usuallyHiddenTextView;
    public EARVListItemExpansionDelegate delegate;

    private float originalHeight;
    private float expandedHeight;

    public EARVListItemViewHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

        this.rootView = itemView;
        this.alwaysShownTextView = (TextView) itemView.findViewById(R.id.always_showing_text_view);
        this.usuallyHiddenTextView = (TextView) itemView.findViewById(R.id.sometimes_i_hide);

        itemView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        this.originalHeight = itemView.getMeasuredHeight();

        this.usuallyHiddenTextView.setVisibility(View.VISIBLE);
        itemView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        this.expandedHeight = itemView.getMeasuredHeight();

        this.usuallyHiddenTextView.setVisibility(View.GONE);
    }

    @Override
    public void onClick(final View v) {
        boolean isExpanded = (this.usuallyHiddenTextView.getVisibility() == View.VISIBLE);
        this.usuallyHiddenTextView.setVisibility((isExpanded) ? View.GONE : View.VISIBLE);

        //Not a huge fan of this but I suppose its functional for now and better than boring magic numbers

        ValueAnimator valueAnimator = ValueAnimator.ofFloat(v.getHeight(), (isExpanded) ? this.originalHeight : this.expandedHeight);
        valueAnimator.setDuration(100);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = (float) animation.getAnimatedValue();
                v.getLayoutParams().height = (int) value;
                v.requestLayout();
            }
        });
        valueAnimator.start();

        if (this.delegate != null) {
            this.delegate.itemStateChanged(this, !isExpanded);
        }
    }

    public void setViewExpanded(final boolean expanded) {
        if (this.rootView != null && this.rootView.getLayoutParams() != null) {
            this.rootView.getLayoutParams().height = (int) ((expanded) ? this.expandedHeight : this.originalHeight);
            this.rootView.requestLayout();
        }
    }
}
